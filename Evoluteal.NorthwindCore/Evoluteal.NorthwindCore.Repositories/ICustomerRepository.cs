﻿using Evoluteal.NorthwindCore.Models;
using System.Collections.Generic;

namespace Evoluteal.NorthwindCore.Repositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        IEnumerable<CustomerList> CustomerPagedList(int page, int rows);
    }
}
