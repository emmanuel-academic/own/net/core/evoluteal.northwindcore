﻿using System.Collections.Generic;

namespace Evoluteal.NorthwindCore.Repositories
{
    public interface IRepository<T> where T : class
    {
        bool Delete(T entity);
        bool Update(T entity);
        long Insert(T entity);
        IEnumerable<T> GetList();
        T GetByID(int id);
    }
}
