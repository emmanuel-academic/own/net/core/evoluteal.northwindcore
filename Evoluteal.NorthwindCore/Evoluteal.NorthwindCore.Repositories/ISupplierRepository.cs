﻿using Evoluteal.NorthwindCore.Models;
using System.Collections.Generic;

namespace Evoluteal.NorthwindCore.Repositories
{
    public interface ISupplierRepository : IRepository<Supplier>
    {
        IEnumerable<Supplier> SupplierPagedList(int page, int rows);
    }
}
