﻿using Evoluteal.NorthwindCore.Models;
using System.Collections;

namespace Evoluteal.NorthwindCore.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        User ValidateUser(string email, string password);
    }
}
