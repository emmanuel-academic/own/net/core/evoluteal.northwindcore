﻿using Evoluteal.NorthwindCore.Repositories;

namespace Evoluteal.NorthwindCore.UnitOfWork
{
    public interface IUnitOfWork
    {
        ICustomerRepository Customer { get; }
        IUserRepository User { get; }
        ISupplierRepository Supplier { get; }
    }
}
