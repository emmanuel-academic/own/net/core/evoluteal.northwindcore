﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Evoluteal.NorthwindCore.Models;
using Evoluteal.NorthwindCore.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Evoluteal.NorthwindCore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CustomerController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public CustomerController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        [HttpGet]
        [Route("{id:int}")]
        public IActionResult GetById(int id)
        {
            return this.Ok(this._unitOfWork.Customer.GetByID(id));
        }

        [HttpGet]
        [Route("GetPaginatedCustomer/{page:int}/{rows:int}")]
        public IActionResult GetPaginatedCustomer(int page, int rows)
        {
            return this.Ok(this._unitOfWork.Customer.CustomerPagedList(page, rows));
        }

        [HttpPost]
        public IActionResult Post([FromBody]Customer customer)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest();
            }
            else
            {
                return this.Ok(this._unitOfWork.Customer.Insert(customer));
            }
        }

        [HttpPut]
        public IActionResult Put([FromBody]Customer customer)
        {
            if (this.ModelState.IsValid && this._unitOfWork.Customer.Update(customer))
            {
                return this.Ok(new { Message = "Customer was Updated" });
            }
            else
            {
                return this.BadRequest();
            }
        }

        [HttpDelete]
        public IActionResult Delete([FromBody]Customer customer)
        {
            if (customer.Id > 0)
            {
                return this.Ok(this._unitOfWork.Customer.Delete(customer));
            }
            else
            {
                return this.BadRequest();
            }
        }
    }
}