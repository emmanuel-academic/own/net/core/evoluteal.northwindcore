﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Evoluteal.NorthwindCore.Models;
using Evoluteal.NorthwindCore.UnitOfWork;
using Evoluteal.NorthwindCore.WebApi.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Evoluteal.NorthwindCore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {

        private readonly ITokenProvider _tokenProvider;
        private readonly IUnitOfWork _unitOfWork;


        public TokenController(ITokenProvider tokenProvider, IUnitOfWork unitOfWork)
        {
            this._tokenProvider = tokenProvider;
            this._unitOfWork = unitOfWork;
        }

        [HttpPost]
        public JsonWebToken Post([FromBody]User userLogin)
        {
            var user = this._unitOfWork.User.ValidateUser(userLogin.Email, userLogin.Password);

            if (user is null)
            {
                throw new UnauthorizedAccessException();
            }

            var token = new JsonWebToken
            {
                Access_Token = this._tokenProvider.CreateToken(user, DateTime.UtcNow.AddHours(8)),
                Expires_In = 480//minutes
            };
            return token;
        }
    }
}