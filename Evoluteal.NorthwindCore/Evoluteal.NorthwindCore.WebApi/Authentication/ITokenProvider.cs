﻿using Evoluteal.NorthwindCore.Models;
using Microsoft.IdentityModel.Tokens;
using System;

namespace Evoluteal.NorthwindCore.WebApi.Authentication
{
    public interface ITokenProvider
    {
        string CreateToken(User user, DateTime expiry);
        TokenValidationParameters GetValidationParameters();
    }
}
