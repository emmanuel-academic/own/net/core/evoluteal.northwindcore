﻿using Evoluteal.NorthwindCore.Repositories;
using Evoluteal.NorthwindCore.UnitOfWork;

namespace Evoluteal.NorthwindCore.DataAccess
{
    public class NorthwindUnitOfWork : IUnitOfWork
    {
        public NorthwindUnitOfWork(string connectionString)
        {
            this.Customer = new CustomerRepository(connectionString);
            this.User = new UserRepository(connectionString);
            this.Supplier = new SupplierRepository(connectionString);
        }

        public ICustomerRepository Customer { get; private set; }
        public IUserRepository User { get; private set; }
        public ISupplierRepository Supplier { get; private set; }
    }
}
