﻿using Dapper.Contrib.Extensions;
using Evoluteal.NorthwindCore.Repositories;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Evoluteal.NorthwindCore.DataAccess
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected string _connectionString;

        public Repository(string connectionString)
        {
#pragma warning disable S3010 // Static fields should not be updated in constructors
            SqlMapperExtensions.TableNameMapper = (type) => { return $"{ type.Name}"; };
#pragma warning restore S3010 // Static fields should not be updated in constructors
            this._connectionString = connectionString;
        }

        public bool Delete(T entity)
        {
            using (var connection = new SqlConnection(this._connectionString))
            {
                return connection.Delete(entity);
            }
        }

        public T GetByID(int id)
        {
            using (var connection = new SqlConnection(this._connectionString))
            {
                return connection.Get<T>(id);
            }
        }

        public IEnumerable<T> GetList()
        {
            using (var connection = new SqlConnection(this._connectionString))
            {
                return connection.GetAll<T>();
            }
        }

        public long Insert(T entity)
        {
            using (var connection = new SqlConnection(this._connectionString))
            {
                return connection.Insert(entity);
            }
        }

        public bool Update(T entity)
        {
            using (var connection = new SqlConnection(this._connectionString))
            {
                return connection.Update(entity);
            }
        }
    }
}
