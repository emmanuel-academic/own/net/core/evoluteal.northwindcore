﻿using Dapper;
using Evoluteal.NorthwindCore.Models;
using Evoluteal.NorthwindCore.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace Evoluteal.NorthwindCore.DataAccess
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(string connectionString) : base(connectionString)
        {
        }

        public User ValidateUser(string email, string password)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@email", email);
            parameters.Add("@password", password);

            using (var connection = new SqlConnection(this._connectionString))
            {
                return connection.QueryFirstOrDefault<User>("dbo.ValidateUser",
                    parameters,
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}
