﻿using Dapper;
using Evoluteal.NorthwindCore.Models;
using Evoluteal.NorthwindCore.Repositories;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Evoluteal.NorthwindCore.DataAccess
{
    public class SupplierRepository : Repository<Supplier>, ISupplierRepository
    {
        public SupplierRepository(string connectionString) : base(connectionString)
        {
        }

        public IEnumerable<Supplier> SupplierPagedList(int page, int rows)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@page", page);
            parameters.Add("@rows", rows);

            using (var connection = new SqlConnection(this._connectionString))
            {
                return connection.Query<Supplier>("dbo.SupplierPagedList",
                    parameters,
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}
