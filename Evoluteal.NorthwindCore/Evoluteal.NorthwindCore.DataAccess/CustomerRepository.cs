﻿using Dapper;
using Evoluteal.NorthwindCore.Models;
using Evoluteal.NorthwindCore.Repositories;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Evoluteal.NorthwindCore.DataAccess
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(string connectionString) : base(connectionString)
        {
        }

        public IEnumerable<CustomerList> CustomerPagedList(int page, int rows)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@page", page);
            parameters.Add("@rows", rows);

            using (var connection = new SqlConnection(this._connectionString))
            {
                return connection.Query<CustomerList>("dbo.CustomerPagedList",
                    parameters,
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}
